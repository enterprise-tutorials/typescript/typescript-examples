# Different examples using TypeScript

## Setup & Run
* Install TypeScript
```bash
npm install -g typescript
```

* Compile code.
This will generate `.js` files.
```bash
tsc src/01_basic.ts
```

* View Results (Instructions for Mac Users)
  * Browser: Open HTML file in your default browser
    ```bash
    open index.html
    ```

   * Command Line: Open Terminal
       ```bash
      node src/01_basic.js
      ```

`IMP:` If you face compilation issue with "Promise",
then execute `npm i @types/node` from Terminal.

## References
[TypeScript](https://www.typescriptlang.org/)

