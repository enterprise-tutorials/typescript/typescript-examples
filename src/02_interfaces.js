var PublicCompany = /** @class */ (function () {
    function PublicCompany(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
    return PublicCompany;
}());
function getCompany(company_str) {
    return "Hello, " + company_str.firstName + " " + company_str.lastName;
}
// let company_name = { firstName: "Microsoft", lastName: "Champions" };
var company_name = new PublicCompany("Google", "is", "Inc");
document.body.textContent = getCompany(company_name);
