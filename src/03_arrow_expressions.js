var materials = [
    'Hydrogen',
    'Helium',
    'Lithium',
    'Beryllium'
];
document.body.textContent = materials.map(function (material) { return material.length; }).toString();
console.log(materials.map(function (material) { return material.length; }));
// expected output: Array [8, 6, 7, 9]
