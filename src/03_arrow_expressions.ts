// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions

const materials = [
    'Hydrogen',
    'Helium',
    'Lithium',
    'Beryllium'
];

document.body.textContent = materials.map(material => material.length).toString();
console.log(materials.map(material => material.length));
// expected output: Array [8, 6, 7, 9]
