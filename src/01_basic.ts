function getCompany(varStr: string) {
    return "Hello, " + varStr;
}
function prepVar(varNum: number) {
    return "Hello, " + varNum;
}

// let company_name = "Microsoft Inc";

const hero = {
    name: 'Batman'
};
let var1;
let var2 = 4;
let whichVar = (hero.hasOwnProperty('realName') ? 4 : 6) || var2;

document.body.textContent = prepVar(whichVar);
