interface Company {
    firstName: string;
    lastName: string;
}

class PublicCompany {
    fullName: string;
    constructor(public firstName: string, public middleInitial: string, public lastName: string) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
}

function getCompany(company_str: Company) {
    return "Hello, " + company_str.firstName + " " + company_str.lastName;
}

// let company_name = { firstName: "Microsoft", lastName: "Champions" };
let company_name = new PublicCompany("Google", "is", "Inc");

document.body.textContent = getCompany(company_name);