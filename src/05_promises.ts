// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
// https://medium.com/@JonasJancarik/handling-those-unhandled-promise-rejections-when-using-javascript-async-await-and-ifee-5bac52a0b29f
// https://blog.bitsrc.io/keep-your-promises-in-typescript-using-async-await-7bdc57041308

// A Promise is an object representing the eventual completion or failure of an asynchronous operation.

// Case 1:
const wait = (ms) => new Promise(res => setTimeout(res, ms));
const startAsync = async callback => {
    await wait(1000);
    callback('Hello');
    await wait(1000);
    callback('And Welcome');
    await wait(1000);
    callback('To Async Await Using TypeScript');
};
startAsync(text => console.log(text));

// Case 2:
const one = new Promise<string>((resolve, reject) => {});
one.then(value => {
    console.log('resolved', value);
});
one.catch(error => {
    console.log('rejected', error);
});
